'use strict';

var application = angular.module('offearchApplication', ['ngRoute',
    'ngResource',
    'ui.bootstrap',
    'offearchApplication.controllers',
    'offearchApplication.services',
    'ui.router']);
application.config(['$urlRouterProvider', '$stateProvider', '$locationProvider', '$httpProvider', function ($urlRouterProvider, $stateProvider, $locationProvider, $httpProvider) {

    $stateProvider.state('Application', {
        template: '<ui-view></ui-view>'
    }).state('Application.Home', {
        url: '/',
        templateUrl: 'views/homepage.html',
        controller: 'HomePageController',
        accessLevel: accessLevels.public
    }).state('Application./', {
        url: '',
        controller: 'HomePageController',
        templateUrl: 'views/homepage.html',
        accessLevel: accessLevels.public
    }).state('Application.Offers', {
        url: '/offers/',
        redirectTo: '/search/offers',
        accessLevel: accessLevels.public
    }).state('Application.OneOffer', {
        url: '/offers/:id',
        controller: 'OffersController',
        templateUrl: 'views/item.html',
        accessLevel: accessLevels.public
    }).state('Application.Lookous', {
        url: '/lookouts/',
        redirectTo: '/search/lookouts',
        accessLevel: accessLevels.public
    }).state('Application.OneLookout', {
        url: '/lookouts/:id',
        controller: 'LookoutsController',
        templateUrl: 'views/item.html',
        accessLevel: accessLevels.public
    }).state('Application.AddLookout', {
        url: '/add/lookout',
        controller: 'AddItemsController',
        templateUrl: 'views/addLookout.html',
        accessLevel: accessLevels.public
    }).state('Application.AddOffer', {
        url: '/add/offer',
        controller: 'AddItemsController',
        templateUrl: 'views/addOffer.html',
        accessLevel: accessLevels.user
    }).state('Application.Search', {
        url: '/search/:type',
        controller: 'SearchController',
        templateUrl: 'views/search.html',
        accessLevel: accessLevels.public
    }).state('Application.unauthorized', {
        url: '/unauthorized',
        controller: 'DefaultController',
        templateUrl: 'views/unauthorized.html',
        accessLevel: accessLevels.public
    }).state('Application.Login', {
        url: '/login',
        controller: 'MembershipController',
        templateUrl: 'views/membership.html',
        accessLevel: accessLevels.public
    }).state('Application.Registrations', {
        url: '/registrations',
        controller: 'MembershipController',
        templateUrl: 'views/membership.html',
        accessLevel: accessLevels.public
    }).state('otherwise', {
        templateUrl: 'views/homepage.html',
        controller: 'HomePageController',
        accessLevel: accessLevels.public
    });
    
    $urlRouterProvider.otherwise('/');
       
}]).run(function ($rootScope, membershipService, $state, $location, $timeout) {
  
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        // if access is not granted..
        if(!membershipService.isAccessGranted(toState.accessLevel))
        {
            $timeout(function() {
                $location.path("/unauthorized");
            }, 10);
        }
    });

}).controller('FullSiteController', function ($rootScope,$scope, $state, $stateParams, membershipService, $http, $timeout, $location) {

  
});
