﻿'use strict';

offearchApplicationControllersModule.controller('AddItemsController', ['$scope', '$stateParams', function (scope, $stateParams) {

    scope.offer =
        {
            title: 'd',
            description: 'S',
            seller: 'עמית ש',
            imageSource: 'S',
            price: 11
        };

    scope.paymentType = true;

    scope.paymentTypeChanged = function(paymentType) {
        if (paymentType == 0) {
            scope.paymentType = false;
        } else {
            scope.paymentType = true;
        }
    };

    scope.addOffer = function() {
        console.log(scope.offer);
    };

    scope.cleanForm = function() {
        scope.offer = {
            title: '',
            description: '',
            seller: 'עמית ש',
            imageSource: '',
            price: 0
        };

        if (scope.offer.price = 0) {
            paymentTypeChanged(0);
        }
    };
}]);