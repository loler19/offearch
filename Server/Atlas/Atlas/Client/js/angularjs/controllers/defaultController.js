﻿'use strict';

/*
This is a very very very dummy controller.
Every view which does not supose to have a complex controller, gets this dummy controller.
*/

offearchApplicationControllersModule.controller('DefaultController',
    ['$scope', '$route', '$stateParams', '$location', '$cookies', 'membershipService', '$timeout',
    function ($scope, $route, $stateParams, $location, $cookies, $membershipService, $timeout) {

        $scope.searchType = "3";

        $scope.Search = function(currentSearchTerm) {
            // do the searching logic
            console.log(scope.searchType);
        };

        $scope.changeSearchType = function(number) {
            $scope.searchType = number;
        };

        $scope.$on('$stateChangeSuccess', function(event, next) {
            $scope.isHomePage = $location.path() == '/' || $location.path() == '';
        });

        $scope.userLogged = $membershipService.currentUser().Username;
            
        $membershipService.registerUserStateChanged(function (data) {
            $scope.userLogged = data.Username;
        });

        $scope.logout = function () {
            $membershipService.logout(function () {
                $timeout(function () {
                    $location.path("/");
                }, 200);
            });
        };
        $scope.moveToLogin = function() {
            $location.path("/login");
        };
    }]);
