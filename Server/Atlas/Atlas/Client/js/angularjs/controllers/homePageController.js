﻿'use strict';

offearchApplicationControllersModule.controller('HomePageController',
    ['$scope', '$stateParams', 'itemsService', function($scope, $stateParams, $itemsService) {
        // fetch the 4 items.
        $itemsService.ItemsBySearch(1, 0, 4, new Array(), new Array(), function (callbackData) {
            $scope.offers = callbackData;
            $scope.$apply();
        });
        
        // fetch the 4 items.
        $itemsService.ItemsBySearch(2, 0, 4, new Array(), new Array(), function (callbackData) {
            $scope.lookers = callbackData;
            $scope.$apply();
        });
    }]);
