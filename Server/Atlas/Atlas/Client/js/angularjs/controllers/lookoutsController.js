﻿'use strict';

offearchApplicationControllersModule.controller('LookoutsController',
    ['$scope', '$stateParams', 'itemsService', 'questionsService', function ($scope, $stateParams, $itemsService, $questionsService) {
    // lookoutType is 1
    // Lets say that the item supose to be found.
    $scope.itemNotFound = false;

    if ($stateParams.id != undefined) {
        $itemsService.ItemById($stateParams.id).query(function (callbacksData) {
            // get the item from our service
            var fectchedItem = callbacksData;
            // if we did not find the item, or the item is not OFFER
            if (fectchedItem.null == true || fectchedItem.ItemType != 2) {
                $scope.itemNotFound = true;
            } else {
                $scope.item = callbacksData;
            }

        }, function (failer) {
            $scope.itemNotFound = true;
        });

        return;
    }
}]);
