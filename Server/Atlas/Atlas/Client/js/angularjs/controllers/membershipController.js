﻿'use strict';

offearchApplicationControllersModule.controller('MembershipController', ['$scope', '$stateParams', 'membershipService', '$location', '$timeout',
    function ($scope, $stateParams, $membershipService, $location, $timeout) {

    $scope.isLoginSuccssed = false;
    $scope.isLoginFailed = false;
    $scope.isRegistrationSuccssed = false;
    $scope.isRegistrationFailed = false;

    // Login function.
    $scope.login = function () {
        var username = $scope.loginUser.username;
        var password = $scope.loginUser.password;
        $membershipService.login(username, password, function (isLogged) {
            // login was successfull
            if (isLogged) {
                $scope.isLoginSuccssed = true;
                $timeout(function () {
                    $location.path("/");
                }, 1500);
            } else {
                $scope.isLoginSuccssed = false;
                $scope.isLoginFailed = true;
                $scope.isRegistrationFailed = false;
            }
        });
    };
    
    // Register function
    $scope.register = function () {
        var username = $scope.registerUser.username;
        var password = $scope.registerUser.password;
        var email = $scope.registerUser.email;

        $membershipService.register(username, password, email, function (isRegistered) {
            // registration was successfull
            if (isRegistered) {
                $scope.isRegistrationSuccssed = true;
            } else {
                $scope.isRegistrationSuccssed = false;
                $scope.isRegistrationFailed = true;
                $scope.isLoginFailed = false;
            }
        });
    };
}]);
