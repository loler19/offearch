﻿'use strict';

offearchApplicationControllersModule.controller('OffersController', ['$scope', '$stateParams', 'itemsService', 'questionsService','membershipService', function ($scope, $stateParams, $itemsService, $questionsService, $membershipService) {
    // lookoutType is 0
    // Lets say that the item supose to be found.
    $scope.itemNotFound = false;
    
    if ($stateParams.id != undefined) {

        $itemsService.ItemById($stateParams.id).fetch(function (callbacksData) {
            // get the item from our service
            var fectchedItem = callbacksData;
            // if we did not find the item, or the item is not OFFER
            if (fectchedItem.null == true || fectchedItem.ItemType != 1) {
                $scope.itemNotFound = true;
            } else {
                $scope.item = callbacksData;

                $scope.isItemCreatorIsTheCurrentUser = function () {
                    return $membershipService.currentUser().Username == callbacksData.Creator;
                };
            }

        }, function (failer) {
            $scope.itemNotFound = true;
        });


        
        return;
    }

}]);
