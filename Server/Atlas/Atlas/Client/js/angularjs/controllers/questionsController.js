﻿'use strict';

offearchApplicationControllersModule.controller('QuestionsController', ['$scope', '$modal', '$stateParams', 'questionsService', 'membershipService', function ($scope, $modal, $stateParams, $questionsService, $membershipService) {

    if ($stateParams.id != undefined) {
        // fetch the questions into the current view
        getQuestions($stateParams.id);
        // intialize new question
        $scope.newQuestion = new Question("", "", "", $stateParams.id);
    }

    // show the modal of adding question window
    $scope.showAddQuestionModal = function (titleOfCurrentItem) {
        $scope.opts = {
            backdrop: true,
            backdropClick: true,
            dialogFade: false,
            keyboard: true,
            templateUrl: '../client/views/addQuestionModal.html',
            controller: addNewQuestionModalController,
            resolve: { } // empty storage
        };

        $scope.opts.resolve.item = function() {
            return angular.copy({ newQuestion: $scope.newQuestion, questionsService: $questionsService, membershipService:$membershipService, titleOfCurrentItem: titleOfCurrentItem });
        };

        var addQuestionModalInstance = $modal.open($scope.opts);

        addQuestionModalInstance.result.then(function () {
            //on ok button press 
            getQuestions($stateParams.id);
        }, function () {
            //on cancel button press
        });
    };
    
    // show the modal of answer question window
    $scope.showAnswerQuestionModal = function (questionToAnswer) {
        $scope.opts = {
            backdrop: true,
            backdropClick: true,
            dialogFade: false,
            keyboard: true,
            templateUrl: '../client/views/answerQuestionModal.html',
            controller: answerQuestionModalController,
            resolve: {} // empty storage
        };

        $scope.opts.resolve.item = function () {
            return angular.copy({ questionsService: $questionsService, membershipService: $membershipService, question: questionToAnswer });
        };

        var answerQuestionModalInstance = $modal.open($scope.opts);

        answerQuestionModalInstance.result.then(function () {
            //on ok button press 
            getQuestions($stateParams.id);
        }, function () {
            //on cancel button press
        });
    };

    // get the questions from the server.
    function getQuestions(itemId) {
        $questionsService.QuestionsByItemId(itemId).fetch(function (callbackData) {
            $scope.questions = callbackData;
        }, function (failer) {
            // do nothing.
        });
    }
}]);

// Add new question controller
var addNewQuestionModalController = function ($scope, $modalInstance, $modal, item) {
    
    // Initialize couple of things.. :)
    $scope.newQuestion = item.newQuestion;
    $scope.errorOccured = false;
    $scope.inProgress = true;
    $scope.logged = item.membershipService.currentUser().Username != undefined;

    $scope.title = item.titleOfCurrentItem;

    // user clicked ok
    $scope.ok = function () {
        item.questionsService.AddQuestion($scope.newQuestion.Title, $scope.newQuestion.QuestionContent, "", $scope.newQuestion.ItemId, item.membershipService.currentUser().Username).
            success(function(data, status, headers, config) {
                $scope.inProgress = false;
            }).
            error(function (data, status, headers, config) {
                $scope.errorOccured = true;
            });

        setTimeout(function() {
            $modalInstance.close();
        }, 1000);
    };

    // user canceld
    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
};

// Answer question controller
var answerQuestionModalController = function ($scope, $modalInstance, $modal, item) {

    // Initialize couple of things.. :)
    $scope.question = item.question;
    $scope.errorOccured = false;
    $scope.inProgress = true;
    $scope.logged = item.membershipService.currentUser().Username != undefined;
    
    // user clicked ok
    $scope.ok = function () {
        item.questionsService.AnswerQuestion($scope.question.ItemId, $scope.question.Id, $scope.question.Answer).
            success(function (data, status, headers, config) {
                $scope.inProgress = false;
            }).
            error(function (data, status, headers, config) {
                $scope.errorOccured = true;
            });

        setTimeout(function () {
            $modalInstance.close();
        }, 1000);
    };

    // user canceld
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};
