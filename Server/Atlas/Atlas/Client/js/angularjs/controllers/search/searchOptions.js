﻿function searchOption(included) {

    this.included = included;

    this.queryIt = function (queryData) {
        return queryData;
    };

    this.toString = function () {
        return "empty option";
    };
}

function startingPriceOption(startingPrice) {

    this.startingPrice = startingPrice;

    this.queryIt = function (queryData) {
        queryData.queryParts.push(new QueryPart("Price", "Bigger", this.startingPrice.toString(), "Number"));
        return queryData;
    };

    this.toString = function () {
        if (this.startingPrice != 0) {
            return "ממחיר " + this.startingPrice;
        }
        return "";
    };

}

startingPriceOption.prototype = new searchOption(false);

function limitPriceOption(limitPrice) {

    this.limitPrice = limitPrice;

    this.queryIt = function (queryData) {
        queryData.queryParts.push(new QueryPart("Price", "Smaller", this.limitPrice.toString(), "Number"));
        return queryData;
    };

    this.toString = function () {
        if (this.limitPrice != 0) {
            return "עד מחיר " + this.limitPrice;
        }
        return "";
    };

}

limitPriceOption.prototype = new searchOption(false);

function isOffersOption() {

    this.queryIt = function (queryData) {
        queryData.type = queryData.type | 1;
        return queryData;
    };

    this.toString = function () {
        if (this.included) {
            return "כולל הצעות";
        }
        return "";
    };
}

isOffersOption.prototype = new searchOption(false);

function isLookoutsOption() {

    this.queryIt = function (queryData) {
        queryData.type = queryData.type | 2;
        return queryData;
    };

    this.toString = function () {
        if (this.included) {
            return "כולל מודעות חיפוש";
        }
        return "";
    };
}

isLookoutsOption.prototype = new searchOption(false);

function tagsOption(tags) {
    
    this.tags = tags;

    this.queryIt = function (queryData) {
        queryData.tags = this.tags;
        
        return queryData;
    };
    
    this.toString = function () {
        return "תגים";
    };
}

tagsOption.prototype = new searchOption(false);