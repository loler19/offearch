﻿'use strict';

// ReSharper disable InconsistentNaming

offearchApplicationControllersModule.controller('SearchController', ['$scope', '$stateParams', 'itemsService', '$timeout', '$location', function($scope, $stateParams, $itemsService, $timeout, $location) {

    // the parameters that will be sent to the search service.
    var searchParams =
        {
            type: 0, // ALL == 3
            offset: 0, // start point
            limit: 3, // how much
            tags: new Array(),
            queryParts: new Array()
        };

    // initialize tags.
    $(document).ready(function () {
        var itemsToBring = 10;
        var offset = 0;
        var autoCompleteType = 0;
        $("#demo-input").tokenInput("http://localhost:36938/api/autocomplete/?autoCompleteType=" + autoCompleteType + "&amount=" + itemsToBring + "&offset=" + offset, {
            queryParam: "term",
            propertyToSearch: "Value",
            preventDuplicates: true,
            tokenValue: "Id",
            theme: "facebook",
            searchingText: "מחפש...",
            hintText: "אנא הקש מילות חיפוש",
            onResult: function (results, query) {

                var ifItemExistsInTheDropDownList = _.find(results, function (item) {
                    return (item.Value.toLowerCase() == query.toLowerCase() && item.Type == autoCompleteType);
                });

                // if it's 1 or bigger.
                if (ifItemExistsInTheDropDownList) {
                    return results;
                }

                return results.concat({ Id: -1, Value: query, Type: autoCompleteType });
            }
        });
    });

    var initializeSearchOptions = function() {
        if ($stateParams != null) {
            try {
                var options = JSON.parse($stateParams.type);

                $scope.options =
                    {
                        // using _.extend is very cool idea - in order to keep the type of the object,
                        // we will create the object from scratch and copy the fields from the json we got.
                        startingPriceOption: _.extend(new startingPriceOption(0), options.startingPriceOption),
                        limitPriceOption: _.extend(new limitPriceOption(0), options.limitPriceOption),
                        isOffersOption: _.extend(new isOffersOption(), options.isOffersOption),
                        isLookoutsOption: _.extend(new isLookoutsOption(), options.isLookoutsOption),
                        tagsOption: _.extend(new tagsOption(new Array()), options.tagsOption)
                    };
            }
                // if any error occured, lets just set the default options.
            catch(e) {

                $scope.options =
                    {
                        startingPriceOption: new startingPriceOption(0),
                        limitPriceOption: new limitPriceOption(0),
                        isOffersOption: new isOffersOption(false),
                        isLookoutsOption: new isLookoutsOption(false),
                        tagsOption: new tagsOption(new Array())
                    };
            }
        }

        $scope.oldOptionsTitles = new Array();

        _.each($scope.options, function(item) {
            if (item.included == true) {
                $scope.oldOptionsTitles.push(item.toString());
                searchParams = item.queryIt(searchParams);
            }
        });

        // add the tags to the control.
        _.each($scope.options.tagsOption.tags, function (item) {
            $("#demo-input").tokenInput("add", { Id: item.Id, Type:item.Type, Value: item.Value });
        });
    };

    initializeSearchOptions();

    $scope.isFetchingItems = true;
    $scope.noMoreItems = false;
    $scope.noItemsFound = false;

    // fetch the 4 items.
    $itemsService.ItemsBySearch(searchParams.type, searchParams.offset, searchParams.limit, searchParams.tags, searchParams.queryParts, function(callbackData) {
        $scope.isFetchingItems = false;
        $scope.items = callbackData;

        if (callbackData.length == 0) {
            $scope.noItemsFound = true;
        }

        searchParams.offset += 3;

        $scope.$apply();
    });

    $scope.fetchMoreItems = function() {
        // fetch the 4 items.
        $scope.isFetchingItems = true;

        $itemsService.ItemsBySearch(searchParams.type, searchParams.offset, searchParams.limit, searchParams.tags, searchParams.queryParts, function(callbackData) {
            $scope.isFetchingItems = false;
            $scope.items = $scope.items.concat(callbackData);
            searchParams.offset += 3;

            $timeout(function() { // wait for DOM, then restore scroll position
                $("html, body").animate({ scrollTop: $(window).scrollTop() + $(window).height() });
            }, 150);

            if (callbackData.length == 0) {
                $scope.noMoreItems = true;
            }

            $scope.$apply();
        });
    };

    $scope.search = function () {
        $scope.options.tagsOption.tags = $("#demo-input").tokenInput("get");
        console.log($scope.options);
        $location.path("/search/" + JSON.stringify($scope.options));
    };
}]);


// ReSharper restore InconsistentNaming