﻿application.filter('emptyToStringFilter', [function () {
    return function (items) {
        return _.filter(items, function (item) {
            var str = item.toString();
            if (str == undefined || str == '') {
                return false;
            }
            return true;
        }).reverse();
    };
}]);
