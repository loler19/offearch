﻿'use strict';

offearchApplicationServiesModule.factory('itemsService', ['$resource', '$http', function ($resource, $http) {
    return {
        
        ItemsBySearch: function(type, offset, limit, tags, queryParts, successCallback) {
            var itemQuery = new Query(type, offset, limit, tags, queryParts);
            $.ajax("http://localhost:36938/api/items/search/", {
                data: itemQuery,
                contentType: 'application/json',
                type: 'GET',
                success: successCallback,
                isArray:true
            });
            
            //return $resource('http://localhost:36938/api/items/search/', {},
            //    {
            //        fetch: { method: 'GET', params: itemQuery, contentType: 'application/json; charset=utf-8', isArray: true }
            //    });
        },

        ItemById: function(id) {
            return $resource('http://localhost:36938/api/items/', {},
                {
                    fetch: {
                        method: 'GET',
                        params: { 'id': id },
                        contentType: 'application/json',
                        isArray: false,
                        transformResponse: function(data, headersGetter) {
                            if (data === 'null') {
                                return { null: true };
                            }
                            return angular.fromJson(data);
                        }
                    }
                });
        }
    };
}]);

function QueryPart(field, relation, value, valueType) {
    var self = this;

    self.Field = field;
    self.Value = value;
    self.Relation = relation;
    self.ValueType = valueType;
}

// DataModel
// The type - offer or lookout, offset - where to begin, limit - howmuch, tags - tags.. , queryparts - the queryparts;
function Query(type, offset, limit, tags, queryParts) {
    var self = this;

    self.ItemType = type;
    self.Offset = offset;
    self.Limit = limit;
    self.Tags = tags;
    self.QueryParts = queryParts;
}

