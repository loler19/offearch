﻿'use strict';

offearchApplicationServiesModule.factory('membershipService', ['$resource', '$http', '$cookies', '$rootScope', function ($resource, $http, $cookies, $rootScope) {

    var cookiesChangedCallbacks = new Array();

    function notifyObserversUserStateChanged() {
        
        var data = $cookies.user;

        if ($cookies.user == undefined) {
            data = { "Username": undefined, 'Role': '' };
        } else {
            data = JSON.parse(data);
        }

        // callbacks shall be called :-)
        _.each(cookiesChangedCallbacks, function (obserever) {
            obserever(data);
        });
    }

    $rootScope.$watch(function () { return $cookies.user; }, function (newValue) {
        notifyObserversUserStateChanged();
    });
    
    $rootScope.$watch(function () { return $cookies.token; }, function (newValue) {
        notifyObserversUserStateChanged();
    });
    
    return {
        
        login: function(username, password, callback) {

            // check the validation of the callback
            if (typeof callback != "function") return;

            // create the userNameLogin object
            var usernameLogin = new LoginUserRequest(username, password);

            $http.post('http://localhost:36938/api/membership/login', JSON.stringify(usernameLogin)).success(function (data, status, headers, config) {
                callback(true);
            }).error(function (data, status, headers, config) {
                callback(false);
            });
        },

        logout: function(callback) {

            // not valid callback.
            if (typeof callback != "function") return;

            // send the request.
            $http.post('http://localhost:36938/api/membership/logout').success(function (data, status, headers, config) {
                callback(true);
            }).error(function(data, status, headers, config) {
                callback(false);
            });
        },

        register: function(username, password, email, callback) {

            // not valid callback.
            if (typeof callback != "function") return;

            // create the userNameRegister object
            var usernameRegister = new User(username, password, email);

            // send the request to the server
            $http.post('http://localhost:36938/api/membership/register', JSON.stringify(usernameRegister)).success(function (data, status, headers, config) {
                callback(true);

            }).error(function(data, status, headers, config) {
                callback(false);
            });
        },

        currentUser: function () {
            
            var data = $cookies.user;
            if ($cookies.user == undefined) {
                data =  { "Username": undefined, 'Role': '' };
            } else {
                data = JSON.parse(data);
            }

            
            return data;
        },
        
        isAccessGranted: function(acsessLevel) {

            if (acsessLevel.title == 'public' || acsessLevel.title == '*') {
                return true;
            }

            if (acsessLevel.title == 'user' && this.currentUser().Role == 2)
            {
                return true;
            }

            return false;
        },
        
        // Occurs when there is a change in the cookies data.
        registerUserStateChanged: function(callback) {
            cookiesChangedCallbacks.push(callback);
        }
        
    };
}]);

// DataModel
function LoginUserRequest(username, password) {
    var self = this;
    self.Username = username;
    self.Password = password;
}

// DataModel
function User(username, password, email) {
    var self = this;
    self.Username = username;
    self.Password = password;
    self.Email = email;
}