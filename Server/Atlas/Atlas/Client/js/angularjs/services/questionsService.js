﻿'use strict';

offearchApplicationServiesModule.factory('questionsService', ['$resource', '$http', function ($resource, $http) {
    return {

        QuestionsByItemId: function (id) {
            return $resource('http://localhost:36938/api/questions/itemid/' + id, {},
                {
                    fetch: {
                        method: 'GET',
                        contentType: 'application/json;  charset=utf-8',
                        isArray: true,
                        transformResponse: function (data, headersGetter) {
                            if (data === 'null') {
                                return { null: true };
                            }
                            return angular.fromJson(data);
                        }
                    }
                    
                });
        },
        
        AddQuestion: function (title, questionContent, answer, itemId, userAsk) {
            var question = new Question(title, questionContent, answer, itemId, userAsk);
            console.log(question);
            return $http.post('http://localhost:36938/api/questions/insert', JSON.stringify(question));
        },
        
        AnswerQuestion: function (itemId, questionId, answer) {
            return $http.post('http://localhost:36938/api/questions/update', JSON.stringify({
                questionId: questionId,
                itemId: itemId,
                answer: answer
            }));
        }        
    };
}]);

// DataModel
function Question(title, questionContent, answer, itemId, userAsk) {
    var self = this;
    self.ItemId = parseInt(itemId);
    self.Answer = answer;
    self.QuestionContent = questionContent;
    self.Title = title;
    self.UserAsk = userAsk;
}