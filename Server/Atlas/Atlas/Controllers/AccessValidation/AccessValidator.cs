﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TokenAuthorization.Core.Account;

namespace Atlas.Controllers.AccessValidation
{
    public class AccessValidator : IAccessValidator
    {
        public bool Validate(string key, UserMetadata userMetadata)
        {
            // if the current user is admin, or the user is the same as it should be.
            if (userMetadata.Role == UserRole.Admin ||
                (userMetadata.IsAuthenticated && userMetadata.Username == key))
            {
                return true;
            }

            return false;
        }
    }

    public interface IAccessValidator
    {
        /// <summary>
        /// Returns true if the user is validate, false othewise.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="userMetadata"> </param>
        /// <returns></returns>
        bool Validate(string key, UserMetadata userMetadata);
    }
}