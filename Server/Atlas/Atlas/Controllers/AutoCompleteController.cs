﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Atlas.Database;
using Atlas.Database.Interfaces;
using Atlas.Models;
using AttributeRouting.Web.Http;

namespace Atlas.Controllers
{
    /// <summary>
    /// This controller is user ONLY for fetching tags items.
    /// Every tag has Name (can be empty), and a Key.
    /// </summary>
    public class AutoCompleteController : ApiController
    {
        private static Dictionary<int, Tag> _tags;

        static AutoCompleteController()
        {
            // initialize the data from the db.

            ICrudProvider crudProvider = new Repository();

            using (crudProvider)
            {
                _tags = crudProvider.Queryable<Tag>().Select().ToDictionary(tag => tag.Id);
            }
        }

        [GET("api/autocomplete/{autoCompleteType}/{term}/{amount}/{offset}")]
        public IEnumerable<AutoCompleteItem> Get(AutoCompleteType autoCompleteType, string term, int amount, int offset)
        {
            var autoCompleteItems = _tags.Where(pair => pair.Value.Value.ToLower().Contains(term.ToLower()))
                .OrderBy(pair => pair.Value.Value.Length)
                .Skip(offset)
                .Take(amount)
                .Select(pair => new AutoCompleteItem() {Id = pair.Value.Id, Value = pair.Value.Value, Type = autoCompleteType.ToString()});
            return autoCompleteItems;
        }

        // GET api/tags/5
        [GET("api/autocomplete/getbyid/{id:int}")]
        public AutoCompleteItem Get(int id)
        {
            var item = _tags.Single(pair => pair.Key == id).Value;

            return new AutoCompleteItem() {Id = item.Id, Value = item.Value, Type = "Tags"};
        }

    }

    public class AutoCompleteItem
    {
        public string Type { get; set; }

        public string Value { get; set; }

        public int Id { get; set; }
    }


    public enum AutoCompleteType
    {
        Tags = 2
    }
}
