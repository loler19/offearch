﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;
using Atlas.Database;
using Atlas.Database.Interfaces;
using Atlas.Models;
using Atlas.QuerySearch;
using BLToolkit.Data.Sql;
using BLToolkit.DataAccess;
using TokenAuthorization.Core.Account;
using TokenAuthorization.Core.Attributes;
using TokenAuthorization.Core.Controllers;

namespace Atlas.Controllers
{
    public class ItemsController : TokenAuthApiController
    {
        // GET api/<controller>
        public IEnumerable<Item> Get()
        {
            throw new NotSupportedException();
        }

        // GET api/<controller>/5
        public Item Get(int id)
        {
            ICrudProvider crudProvider = new Repository();

            using (crudProvider)
            {
                var items = from item in crudProvider.Queryable<Item>().Select()
                            where item.Id == id
                            select item;

                var itemsList = items.ToList();

                return itemsList.Count == 0 ? null : itemsList.First();
            }
        }

        [HttpGet]
        [System.Web.Http.ActionName("Search")]
        public IEnumerable<Item> Search([FromUri]Query one)
        {
            ICrudProvider crudProvider = new Repository();

            IEnumerable<Item> offers;

            using (crudProvider)
            {
                var query = one.GetQuery();

                offers = crudProvider.Queryable<Item>().Execute(query);
            }

            return offers;
        }

        // POST api/<controller>
        [TokenAuthentication(AccessLevel.User | AccessLevel.Admin)]
        public void Post([FromBody]Item value)
        {
            value.Date = DateTime.Now;

            ICrudProvider crudProvider = new Repository();

            crudProvider.Queryable<Item>().Insert(new[] {value});
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}