﻿using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Security;
using Atlas.Database;
using Atlas.Database.Interfaces;
using Atlas.Models;
using AttributeRouting.Web.Http;
using BLToolkit.DataAccess;
using TokenAuthorization.Core.Account;
using TokenAuthorization.Core.Attributes;
using TokenAuthorization.Core.Controllers;

namespace Atlas.Controllers
{
    public class MembershipController : TokenAuthApiController
    {
        [POST("api/membership/login")]
        [TokenAuthentication(AccessLevel.Public)]
        public HttpResponseMessage Login([FromBody]LoginUserRequest loginUserRequest)
        {
            if (loginUserRequest == null || string.IsNullOrWhiteSpace(loginUserRequest.Username) || string.IsNullOrWhiteSpace(loginUserRequest.Password))
            {
                return Error("Error occured");
            }

            ICrudProvider crudProvider = new Repository();

            using (crudProvider)
            {
                var users = (from user in crudProvider.Queryable<User>().Select()
                             where user.Username == loginUserRequest.Username && user.Password == loginUserRequest.Password
                             select user).ToList();

                // if we have got more than one match, or the less than one - there is a problem
                // and we need to handle it (without letting the user to enter the system).
                if (users.Count != 1)
                {
                    return Error("Error occured");
                }

                var userName = users.First().Username;
                UserData.Username = userName;
                UserData.Role = (int) UserRole.User;
                return Login(int.MaxValue, userName, UserRole.User);
            }
        }

        [POST("api/membership/register")]
        [TokenAuthentication(AccessLevel.Public)]
        public void Register([FromBody]User user)
        {
            if (user.AuthenticationLevel != UserRole.Unknown)
            {
                throw new Exception("Error occured");
            }

            ICrudProvider crudProvider = new Repository();

            crudProvider.Queryable<User>().Insert(new[] {user});
        }

        [HttpPost]
        [TokenAuthentication(AccessLevel.Anonymous)]
        public bool ForgotPassword(string username)
        {
            return true;
        }

        [POST("api/membership/logout")]
        [TokenAuthentication(AccessLevel.User)]
        public virtual HttpResponseMessage LogOut()
        {
            return base.Logout(true);
        }
    }
}
