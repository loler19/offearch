﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;
using Atlas.Controllers.AccessValidation;
using Atlas.Database;
using Atlas.Database.Interfaces;
using Atlas.Models;
using AttributeRouting.Web.Http;
using BLToolkit.DataAccess;
using TokenAuthorization.Core.Account;
using TokenAuthorization.Core.Attributes;
using TokenAuthorization.Core.Controllers;

namespace Atlas.Controllers
{
    public class QuestionsController : TokenAuthApiController
    {
        private readonly IAccessValidator _accessValidator;

        public QuestionsController()
        {
            _accessValidator = new AccessValidator();    
        }

        // GET api/questions
        public IEnumerable<string> Get()
        {
            throw new NotSupportedException();
        }

        // GET api/questions/5
        [GET("api/questions/{id:int}")]
        public Question Get(int id)
        {
            ICrudProvider crudProvider = new Repository();

            using (crudProvider)
            {
                var selectedQuestion = from question in crudProvider.Queryable<Question>().Select()
                                       where question.Id == id
                                       select question;

                var itemsList = selectedQuestion.ToList();

                return itemsList.Count == 0 ? null : itemsList.First();
            }
        }

        [GET("api/questions/ItemId/{id:int}")]
        public IEnumerable<Question> GetByItemId(int id)
        {
            ICrudProvider crudProvider = new Repository();

            using (crudProvider)
            {
                var selectedQuestion = from question in crudProvider.Queryable<Question>().Select()
                                       where question.ItemId == id
                                       select question;

                return selectedQuestion.ToList();
            }
        }

        // POST api/questions
        // Only user can post a question.
        [TokenAuthentication(AccessLevel.User | AccessLevel.Admin)]
        [POST("api/questions/insert")]
        public HttpResponseMessage Insert([FromBody]Question value)
        {
            value.DatePosted = DateTime.Now;

            ICrudProvider crudProvider = new Repository();

            crudProvider.Queryable<Question>().Insert(new[] {value});

            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        // POST api/questions
        // Only user can post a question.
        [TokenAuthentication(AccessLevel.User | AccessLevel.Admin)]
        [POST("api/questions/update")]
        public HttpResponseMessage Update([FromBody]dynamic updateData)
        {
            var questionItemCreator = GetItemCreator((int)updateData.itemId);

            if(string.IsNullOrWhiteSpace(questionItemCreator))
            {
                throw new Exception("Item was not found.");
            }

            // if someone updates a question, and it is not the user that offered the item
            // or the admin, we have a problem.
            if (_accessValidator.Validate(questionItemCreator, User) == false)
            {
                return new HttpResponseMessage(HttpStatusCode.Unauthorized);
            }

            // fetch the question from the db
            Question updatedQuestion = GetQuestionById((int)updateData.questionId);

            // update the answer.
            updatedQuestion.Answer = updateData.answer;

            ICrudProvider crudProvider = new Repository();

            crudProvider.Queryable<Question>().Update(new[] {updatedQuestion});

            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        // PUT api/questions/5
        public void Put(int id, [FromBody]string value)
        {

        }

        // DELETE api/questions/5
        public void Delete(int id)
        {

        }

        #region Helpers

        private static string GetItemCreator(int itemId)
        {
            ICrudProvider crudProviderToFindItem = new Repository();

            var questionItemCreator = (from item in crudProviderToFindItem.Queryable<Item>().Select()
                                       where item.Id == itemId
                                       select item.Creator).FirstOrDefault();
            return questionItemCreator;
        }

        private Question GetQuestionById(int questionId)
        {
            ICrudProvider crudProvider = new Repository();

            return (from question in crudProvider.Queryable<Question>().Select()
                    where question.Id == questionId
                    select question).First();
        }

        #endregion
    }
}
