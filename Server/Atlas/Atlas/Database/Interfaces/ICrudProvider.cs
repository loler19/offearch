﻿using System;

namespace Atlas.Database.Interfaces
{
    public interface ICrudProvider : IDisposable
    {
        IQueryableCrud<T> Queryable<T>() where T : class;
    }
}
