﻿using System.Collections.Generic;

namespace Atlas.Database.Interfaces
{
    public interface IDeleter<in T>
    {
        void Delete(IEnumerable<T> items);
    }
}
