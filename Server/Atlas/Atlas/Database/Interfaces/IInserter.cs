﻿using System.Collections.Generic;

namespace Atlas.Database.Interfaces
{
    public interface IInserter<in T>
    {
        void Insert(IEnumerable<T> items);
    }
}
