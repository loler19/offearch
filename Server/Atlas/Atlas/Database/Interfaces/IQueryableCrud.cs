﻿namespace Atlas.Database.Interfaces
{
    public interface IQueryableCrud<T> : IDeleter<T>, IInserter<T>, IUpdater<T>, ISelector<T>
    {
    }
}
