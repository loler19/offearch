﻿using System.Collections.Generic;
using System.Linq;

namespace Atlas.Database.Interfaces
{
    public interface ISelector<out T>
    {
        IQueryable<T> Select();

        IEnumerable<T> Execute(string query);
    }
}
