﻿using System.Collections.Generic;

namespace Atlas.Database.Interfaces
{
    public interface IUpdater<in T>
    {
        void Update(IEnumerable<T> items);
    }
}
