﻿namespace Atlas.Database
{
    public class Prefixes
    {
        public const string ModelTablePrefix = "Table";

        public const string TagsTablePrefix = "ToTags";
    }
}