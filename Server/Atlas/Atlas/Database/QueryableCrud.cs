﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Database.Interfaces;
using BLToolkit.Data;
using BLToolkit.Data.Linq;
using BLToolkit.DataAccess;

namespace Atlas.Database
{
    public class QueryableCrud<T> : IQueryableCrud<T> where T : class
    {
        private readonly DbManager _repository;

        public QueryableCrud(DbManager repository)
        {
            _repository = repository;
        }

        public void Insert(IEnumerable<T> items)
        {
            using (_repository)
            {
                var sqlQuery = new SqlQuery<T>(_repository);

                try
                {
                    sqlQuery.BeginTransaction();
                    sqlQuery.Insert(items);
                    sqlQuery.CommitTransaction();
                }
                catch (Exception)
                {
                    sqlQuery.RollbackTransaction();
                    throw;
                }
            }
        }

        public void Update(IEnumerable<T> items)
        {
            using (_repository)
            {
                var sqlQuery = new SqlQuery<T>(_repository);

                try
                {
                    sqlQuery.BeginTransaction();
                    sqlQuery.Update(items);
                    sqlQuery.CommitTransaction();
                }
                catch (Exception)
                {
                    sqlQuery.RollbackTransaction();
                    throw;
                }
            }
        }

        public void Delete(IEnumerable<T> items)
        {
            using (_repository)
            {
                var sqlQuery = new SqlQuery<T>(_repository);

                try
                {
                    sqlQuery.BeginTransaction();
                    sqlQuery.Delete(items);
                    sqlQuery.CommitTransaction();
                }
                catch (Exception)
                {
                    sqlQuery.RollbackTransaction();
                    throw;
                }
            }
        }

        public IQueryable<T> Select()
        {
            return _repository.GetTable<T>();
        }

        public IEnumerable<T> Execute(string query)
        {
            using (_repository)
            {
                return _repository.SetCommand(query).ExecuteList<T>();
            }
        }
    }
}