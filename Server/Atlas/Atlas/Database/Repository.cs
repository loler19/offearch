﻿using System;
using System.Configuration;
using Atlas.Database.Interfaces;
using Atlas.Models;
using BLToolkit.Data;
using BLToolkit.Data.Linq;

namespace Atlas.Database
{
    public class Repository : DbManager, ICrudProvider
    {
        public Repository()
            : base(new BLToolkit.Data.DataProvider.SqlDataProvider(), ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString)
        {

        }

        public IQueryableCrud<T> Queryable<T>() where T : class
        {
            return new QueryableCrud<T>(this);
        }
    }
}