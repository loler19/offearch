﻿using System;
using Atlas.Database;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Atlas.Models
{
    [TableName("Item" + Prefixes.ModelTablePrefix)]
    public class Item
    {
        [PrimaryKey, NotNull, NonUpdatable]
        public int Id { get; set; }

        public ItemType ItemType { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public float Price { get; set; }

        public string Creator { get; set; }

        public DateTime Date { get; set; }
    }

    [Flags]
    public enum ItemType
    {
        Offer = 1,
        Lookout = 2,
        All = 3
    }
}