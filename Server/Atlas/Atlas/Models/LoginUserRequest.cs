﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Atlas.Models
{
    public class LoginUserRequest
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}