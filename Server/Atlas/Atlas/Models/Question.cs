﻿using System;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Atlas.Models
{
    [TableName("Questions")]
    public class Question
    {
        [PrimaryKey, NotNull, NonUpdatable]
        public int Id { get; set; }

        public string Title { get; set; }

        public string QuestionContent { get; set; }

        public string Answer { get; set; }

        public int ItemId { get; set; }

        public DateTime DatePosted { get; set; }

        public string UserAsk { get; set; }
    }
}