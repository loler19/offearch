﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;

namespace Atlas.Models
{
    [TableName("Tags")]
    public class Tag
    {
        [PrimaryKey, NotNull]
        public int Id { get; set; }

        public string Value { get; set; }
    }
}