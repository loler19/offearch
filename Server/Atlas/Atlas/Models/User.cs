﻿using Atlas.Database;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using TokenAuthorization.Core.Account;

namespace Atlas.Models
{
    [TableName("Users")]
    public class User
    {
        [PrimaryKey, NotNull]
        public string Username { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public UserRole AuthenticationLevel { get; set; }
    }
}