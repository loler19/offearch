﻿namespace Atlas.QuerySearch
{
    public interface IQueryPart
    {
        string GetQuery();
    }
}
