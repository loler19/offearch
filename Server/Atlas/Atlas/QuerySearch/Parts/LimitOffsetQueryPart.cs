﻿namespace Atlas.QuerySearch.Parts
{
    public class LimitOffsetQueryPart : IQueryPart
    {
        private readonly int _limit;
        private readonly int _offset;

        public LimitOffsetQueryPart(int offset, int limit)
        {
            _offset = offset;
            _limit = limit;
        }

        public string GetQuery()
        {
            return string.Format(" OFFSET {0} ROWS FETCH NEXT {1} ROWS ONLY", _offset, _limit);
        }
    }
}