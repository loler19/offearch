﻿namespace Atlas.QuerySearch.Parts
{
    public class OrderByQueryPart : IQueryPart
    {
        private readonly string[] _orderByItems;

        public OrderByQueryPart(string[] orderByItems)
        {
            _orderByItems = orderByItems;
        }

        public string GetQuery()
        {
            return string.Format("ORDER BY {0} DESC", string.Join(",", _orderByItems));
        }
    }
}