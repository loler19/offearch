﻿using System.Collections.Generic;
using System.Linq;
using Atlas.Database;
using Atlas.Models;

namespace Atlas.QuerySearch.Parts
{
    public class TagsQueryPart : IQueryPart
    {
        private readonly Tag[] _tags;
        private readonly string _searchModelType;

        public TagsQueryPart(Tag[] tags, string searchModelType)
        {
            _searchModelType = searchModelType;
            _tags = tags;
        }

        public string GetQuery()
        {
            string tagsJoin = string.Empty;

            if (_tags != null)
            {
                tagsJoin = string.Format("Id in (SELECT A.Id From {0}{1} as A, {0}{1}{2} as B " +
                                         "WHERE A.Id = B.{0}Id " +
                                         "AND B.TagId in ('{3}') " +
                                         "GROUP By A.Id " +
                                         "HAVING Count(A.Id) = {4})", _searchModelType, Prefixes.ModelTablePrefix, Prefixes.TagsTablePrefix,
                                         string.Join("','", _tags.ToList().Select(tag => tag.Id)), _tags.Count());
            } // Order by count(*)

            return tagsJoin;
        }
    }
}