﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Routing;
using Atlas.Database;
using Atlas.Models;
using Atlas.QuerySearch.Parts;

namespace Atlas.QuerySearch
{
    public class Query : IQueryPart
    {
        public ItemType ItemType { get; set; }

        public int Offset { get; set; }

        public int Limit { get; set; }

        public Tag[] Tags { get; set; }

        public List<QueryPart> QueryParts { get; set; }

        public string GetQuery()
        {
            var orderBy = new OrderByQueryPart(new[] {"Date"}).GetQuery();
            var offsetLimit = new LimitOffsetQueryPart(Offset, Limit).GetQuery();
            var tags = new TagsQueryPart(Tags, "Item").GetQuery();

            if (ItemType != ItemType.All)
            {
                if (QueryParts == null || !QueryParts.Any())
                {
                    QueryParts = new List<QueryPart>();
                }

                QueryParts.Add(new QueryPart()
                                   {
                                       Field = "ItemType",
                                       Relation = "Equals",
                                       Value = ((int) ItemType).ToString(CultureInfo.InvariantCulture),
                                       ValueType = "Int"
                                   });
            }
            var simpleQueryParts = string.Empty;
            
            if (QueryParts != null)
            {
                simpleQueryParts = string.Join(" AND ", QueryParts.Select(part => part.GetQuery()));
            }
            if (simpleQueryParts != string.Empty && tags != string.Empty)
            {
                tags = string.Format("AND {0}", tags);
            }

            var whereValue = simpleQueryParts == string.Empty && tags == string.Empty ? string.Empty : "WHERE";

            return string.Format(
                "SELECT * FROM {0}{1} {2} {3} {4} {5} {6}",
                "Item" , Prefixes.ModelTablePrefix, whereValue, simpleQueryParts, tags, orderBy, offsetLimit);
        }

        /// <summary>
        /// Gets the sql query of the query parts
        /// </summary>
        /// <returns></returns>
        private string GetQueryPartsSql()
        {
            var queryPartsSql = string.Join(" AND ", QueryParts.Select(part => part.GetQuery()));
            return queryPartsSql;
        }
    }
}