﻿using System;

namespace Atlas.QuerySearch
{
    public class QueryPart : IQueryPart
    {
        public string Field { get; set; }

        public string Relation { get; set; }

        public string Value { get; set; }

        public string ValueType { get; set; }

        public string GetQuery()
        {
            var args = Relationer.GetRelationString(Relation);

            // string, is, shit.
            if (ValueType == "String")
            {
                if (args == "Contains")
                {
                    return string.Format("{0} LIKE '%{2}%'", Field, args, Value);
                }

                return string.Format("{0} {1} '{2}'", Field, args, Value);
            }

            return string.Format("{0} {1} {2}", Field, args, Value);
        }
    }



}