﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Atlas.QuerySearch
{
    public static class Relationer
    {
        private static NameValueCollection _settings;

        public static string GetRelationString(string relation)
        {
            if(_settings == null)
            {
                _settings =
                    ConfigurationManager.GetSection("atlasCustomConfigurationSectionGroup/relationConfigurationSection")
                    as NameValueCollection;
            }

            return _settings[relation];
        }
    }
}